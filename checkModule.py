from listModule import ReadText


class Check:

    def __init__(self, url2):
        self.url2 = url2
        self.checkPresence()

    def checkPresence(self):
        readResult = ReadText("obecnosc.txt")
        myList = readResult.readFromFile()
        newList = []
        quantity = len(myList)
        x = 0

        while x < quantity:
            print(myList[x])
            presence = input("Obecny? [T/N]")

            if presence.lower() == "t":
                newList.append(myList[x])
                x += 1
            elif presence.lower() == "n":
                x += 1
            else:
                print("Wybierz t lub n")

        f2 = open(self.url2, "w")

        for x in newList:
            f2.write(x)
